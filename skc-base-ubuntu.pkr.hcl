
packer {
  required_plugins {
    amazon = {
      version = ">= 1.1.1"
      source  = "github.com/hashicorp/amazon"
    }
  }
}


source "amazon-ebs" "skc_ubuntu" {
  ami_name      = "skc_base_ubuntu"
  instance_type = "t3.micro"
  source_ami    = "ami-0b3e2bb9a70a08ad3"
  ssh_username  = "admin"
  region        = "us-east-2"
    associate_public_ip_address = true
  tags = {
    Name = "skc_ubuntu_22_4_0.0.0
  }
}


build {
  sources = [
    "source.amazon-ebs.skc_ubuntu"
  ]

  provisioner "shell" {
    #inline = ["sudo apt install -y python3"]
    inline = ["sudo apt-get update", "sudo apt-get upgrade -y", "python3 --version", "sudo apt install -y ansible"]
  }

  provisioner "ansible-local" {
    playbook_file   = "./ansible/build.yml"
    role_paths      = [
      "./ansible/roles/skc-print_os_release",
      "./ansible/roles/skc-update-os",
      "./ansible/roles/skc-add-users",
     ]
  }
}
